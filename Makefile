
all: folder cliente servidor

folder:
	mkdir -p bin obj

cliente: obj/client.o
	gcc obj/client.o -o bin/cliente

servidor: obj/server.o
	gcc obj/server.o -o bin/servidor

obj/client.o: src/client.c include/general.h
	gcc -c src/client.c -o obj/client.o

obj/server.o: src/server.c include/general.h
	gcc -c src/server.c -o obj/server.o

.PHONY: clean

clean:
	rm obj/*.o bin/*