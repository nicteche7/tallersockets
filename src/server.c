#include "../include/general.h"



int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	char c;
	struct sockaddr_in clientaddr;
	char *ip = NULL;
	char *port = NULL;

	while ((c = getopt(argc, argv, "i:p:")) != -1)
	{
		switch (c)
		{
		case 'i':
			ip = optarg;
			break;
		case 'p':
			port = optarg;
			break;
		default:
			perror("Error\n");
			exit(0);
		}
	}

	if (ip == NULL || port == NULL)
	{
		perror("No se ha envidado la ip o el puerto");
		return -1;
	}

	listenfd = open_listenfd(ip, port);
	while (1)
	{
		clientlen = sizeof(clientaddr);
		if ((connfd = accept(listenfd, NULL, 0)) > 0)
		{
			handler_client(connfd);

			close(connfd);
		}
	}
	exit(0);
}

void handler_client(int connfd)
{
	char buffer[MAXSIZE] = {0};
	char respuesta[MAXSIZE] = {0};
	char validador = 1;

	if (recv(connfd, buffer, MAXSIZE, 0) < 0)
	{
		perror("No se recibio mensaje de cliente\n");
		validador = 0;
	}

	int fd2 = open(buffer, O_RDONLY);

	if (fd2 < 0)
	{
		perror("Error al abrir archivo destino\n");
	}
	else
	{

		memset(buffer, 0, MAXSIZE);

		size_t readed, writed, tam;

		while ((readed = read(fd2, buffer, MAXSIZE)) > 0)
		{

			if ((writed = send(connfd, buffer, readed, 0)) < 0)
			{
				perror("Error al escribir en archivo destino");
				break;
			}

			tam += readed;

			memset(buffer, 0, MAXSIZE);
		}
	}

	
}

int open_listenfd(char *ip, char *port)
{
	int puerto = atoi(port);

	struct sockaddr_in direccion_servidor;
	memset(&direccion_servidor, 0, sizeof(direccion_servidor));

	direccion_servidor.sin_family = AF_INET;
	direccion_servidor.sin_port = htons(puerto);

	direccion_servidor.sin_addr.s_addr = inet_addr(ip);

	int listenfd;

	if ((listenfd = socket(direccion_servidor.sin_family, SOCK_STREAM, 0)) < 0)
	{
		perror("Error creando descriptor de socket\n");
		return -1;
	}

	if (bind(listenfd, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor)) < 0)
	{
		perror("Error en el bind\n");
		return -1;
	}

	if (listen(listenfd, QUEUESIZE) < 0)
	{
		perror("Error listen\n");
		return -1;
	}

	return listenfd;
}
