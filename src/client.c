#include "../include/general.h"

int main(int argc, char **argv)
{
	int clientfd;
	int non_option_arguments = 0, index, n;
	char c;
	char *filename = NULL;
	char *host, *port = NULL;
	char buf[MAXSIZE];
	char *pedido = NULL;
	char *ruta = NULL;

	while ((c = getopt(argc, argv, "i:p:R:L:")) != -1)
	{
		switch (c)
		{
		case 'i':
			host = optarg;
			break;
		case 'p':
			port = optarg;
			break;
		case 'R':
			pedido = optarg;
			break;
		case 'L':
			ruta = optarg;
			break;
		default:
			perror("Error");
			exit(0);
		}
	}

	if(host == NULL || port == NULL || pedido == NULL || ruta == NULL){
		perror("Algunas opciones no fueron ingresadas");
		return -1;
	}

	if((clientfd = open_clientfd(host, port)) < 0){
		perror("Error de conexion");
		return -1;
	}

	if(send(clientfd,pedido,strlen(pedido),0) < 0){
		perror("Mensaje no enviado\n");
		close(clientfd);
		return -1;
	}

	size_t readed,writed,tam = 0;

	int fd2 = open(ruta, O_WRONLY | O_CREAT | O_TRUNC, 0666);


	if (fd2 < 0)
	{
		perror("Error al abrir archivo destino\n");
		return -1;
	}


	while((readed = recv(clientfd,buf,MAXSIZE,0)) > 0){
        
        if((writed = write(fd2,buf,readed)) < 0){
            perror("Error al escribir en archivo destino");
            return -1;
        }
        
        if(lseek(fd2,0,SEEK_END) < 0){
            perror("Error al mover puntero interno del archivo destino");
            return -1;
        }
        

        tam += readed;

        memset(buf,0,MAXSIZE);
    }

	printf("Se han guardado %ld bytes recibidos del servidor, y que han sido guardados en %s\n", tam, ruta);

	exit(0);
}

int open_clientfd(char *host, char *port){
	int puerto_server = atoi(port);
	
	struct sockaddr_in servidor;

	memset(&servidor, 0, sizeof(servidor));	
	servidor.sin_family = AF_INET;		
	servidor.sin_port = htons(puerto_server);		
	servidor.sin_addr.s_addr = inet_addr(host) ;

	int clientfd;

	
	if((clientfd = socket(servidor.sin_family, SOCK_STREAM, 0)) < 0){
		perror("Error al crear socket\n");
		return -1;
	}
		
	if(connect(clientfd, (struct sockaddr *)&servidor, sizeof(servidor)) < 0){
		perror("Error conexion\n");
		close(clientfd);
		return -1;
	}

	return clientfd;
}