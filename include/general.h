#include <sys/types.h>
#include <errno.h>
#include <getopt.h>
#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>             
#include <stddef.h>             
#include <string.h>             
#include <unistd.h>             
#include <signal.h>             
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>


#define MAXSIZE 1024

#define QUEUESIZE 1

//int read_response(int fd, char *filename);
void handler_client(int connfd);
int open_clientfd(char *host, char *port);
int open_listenfd(char *ip, char *port);
